package com.tagitech.catsanddogsone;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class OneRestController {

    @GetMapping("/")
    public String helloCatsAndDogs() {
        return "Hello Cats & Dogs";
    }
}
