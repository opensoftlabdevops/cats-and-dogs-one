package com.tagitech.catsanddogsone;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CatsanddogsoneApplication {

	public static void main(String[] args) {
		SpringApplication.run(CatsanddogsoneApplication.class, args);
	}

}
